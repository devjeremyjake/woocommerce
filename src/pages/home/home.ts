import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// import needed file<woocommerce dependency>
import * as WC from 'woocommerce-api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	
	// creating a local Object for module.
	WooCommerce: any;
  products: any[];

  constructor(public navCtrl: NavController) {

  	this.WooCommerce = WC({
  		url: "http://localhost/wordpress",
  		consumerKey: "ck_08411477d75ad1fe0a343bac1391ffa5bf6c719e",
  		consumerSecret: "cs_24e16f506e79778ab91447ba70e66da28d7ddc85"
  	});  
  	
  	this.WooCommerce.getAsync("products").then( (data) => {
            console.log(JSON.parse(data.body));
            this.products = JSON.parse(data.body).products;
  	}, (err) => {
  		console.log(err)
  	})
  }

}
